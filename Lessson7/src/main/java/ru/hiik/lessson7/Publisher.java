/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.hiik.lessson7;

import io.vertx.core.Vertx;

/**
 *
 * @author vaganovdv
 */
public class Publisher
{

   
    // Класс для публикации сообщений
    private Vertx vertx;

    public Publisher(Vertx vertx)
    {
        this.vertx = vertx;
        System.out.println("{Publisher}: получен экземляр vertx");
        
         
        
    }
    
    
    public void publish ()
    {
        vertx.eventBus().publish("S1", "[Publisher] => Привет от {Publisher}");
        vertx.eventBus().publish("S2", "[Publisher] => Привет от {Publisher}");
    }
    
    
  
    
}
