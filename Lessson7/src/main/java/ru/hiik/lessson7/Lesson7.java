
package ru.hiik.lessson7;

import io.vertx.core.Vertx;
import io.vertx.core.eventbus.EventBus;

/**
 *
 * @author vaganovdv
 */
public class Lesson7
{
     public static void main(String[] args)
    {
        
      
        
       Vertx vertx = Vertx.vertx();         // Создан экземпляр класса Vertx
       EventBus eventBus = vertx.eventBus();// 
       
       if (vertx != null)
       {
            Publisher pub = new  Publisher(vertx);
            Subscriber1 sub1 = new Subscriber1(vertx);
            Subscriber2 sub2 = new Subscriber2(vertx);
            
            pub.publish();
            eventBus.publish("S1", "[Lesson7] сообщение");
            eventBus.publish("S2", "[Lesson7] сообщение");
            sub2.publish();
           
           System.out.println("Экземпляр Vertx успешно создан");
       }
       else
       {
           System.out.println("Ошибка: экземпляр Vertx не создан");
       }
    }
}
