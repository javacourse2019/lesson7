/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.hiik.lessson7;

import io.vertx.core.Vertx;

/**
 *
 * @author vaganovdv
 */
public class Subscriber2
{
    private Vertx vertx;

    public Subscriber2(Vertx vertx)
    {
        this.vertx = vertx;
        System.out.println("{Subscriber2}: получен экземляр vertx");
        
         vertx.eventBus().consumer("S2", message ->
        {
            String responce = "[Subscriber2] ***** Привет всем!*****";
            System.out.println("[Subscriber2]: " + message.body());
           
        });
        
    }
    
    public void publish()
    {
        String responce = "Привет всем!";
        vertx.eventBus().publish("S1", responce);
        vertx.eventBus().publish("S2", responce);
       
    }        
    
    
    
}
