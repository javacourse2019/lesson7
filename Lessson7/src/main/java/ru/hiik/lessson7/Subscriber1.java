/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.hiik.lessson7;

import io.vertx.core.Vertx;

/**
 *
 * @author vaganovdv
 */
public class Subscriber1
{
    private Vertx vertx;
    

    public Subscriber1(Vertx vertx)
    {
        this.vertx = vertx;
        System.out.println("{Subscriber1}: получен экземляр vertx");
        
        vertx.eventBus().consumer("S1", message ->
        {
            System.out.println("[Subscriber1]: " + message.body());
        });
        
      
    }
    
    
    
}
